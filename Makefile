fetch_script = fetch-gitlab-comments.ts

clean:
	rm -r public

public/%/main.js: main.js
	cp main.js $(dir $@)

public/%/index.json: ${fetch_script}
	USERNAME=$(notdir $(realpath $(dir $@))); deno run --allow-net --allow-env ${fetch_script} "$${USERNAME}" > "$@"

public/%/index.html: public/%/index.json
	mustache "$<" index.mustache > "$@"

public/%: 
	mkdir -p $@
	make $@/main.js $@/index.json $@/index.html
	@echo ""
	@echo "Now open file:///${PWD}/$@/index.html in your browser"
