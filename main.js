(function(){
  main();

  function debounce(fn, ms) {
    let timeoutId = 0;

    const onCall = (...args) => {
      clearTimeout(timeoutId);

      timeoutId = setTimeout(() => fn(...args), ms);
    };

    return onCall;
  }

  function findSourceTable() {
    return document.querySelector('table');
  }

  function createFilterInput() {
    const input = document.createElement('input');
    input.type = "text";
    input.className = 'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline';
    input.placeholder = 'Filter with regex...';
    return input;
  }

  function createInputTableBody(sourceTable) {
    const sourceTableBody = sourceTable.querySelector('tbody');
    const sourceTableRow = sourceTableBody.querySelector('tr');

    const inputTableBody = sourceTableBody.cloneNode(false);
    const inputTableRow = sourceTableRow.cloneNode(false);
    inputTableBody.appendChild(inputTableRow);

    sourceTableRow.querySelectorAll('td').forEach(td => {
      const inputTd = td.cloneNode(false);
      inputTd.appendChild(createFilterInput());
      inputTableRow.appendChild(inputTd);
    });


    return inputTableBody;
  }

  function createInputTable(sourceTable) {
    const inputTable = sourceTable.cloneNode(false);
    const inputTableHead = sourceTable.querySelector('thead').cloneNode(true);

    inputTable.appendChild(inputTableHead);
    inputTable.appendChild(createInputTableBody(sourceTable));

    return inputTable;
  }

  function createFilterOrNull(value, index) {
    if (!value) {
      return null;
    }

    try {
      const regex = new RegExp(value, 'si');

      return {
        index,
        test: (val) => regex.test(val),
      };
    } catch(e) {
      console.error('Could not create filter with value: ' + value);
      return null;
    }
  }

  function shouldShowRow(row, filters) {
    if (!filters.length) {
      return true;
    }

    return filters.every(x => {
      const text = row.querySelector(`td:nth-child(${x.index + 1})`).textContent;

      return x.test(text);
    });
  }

  function filterSourceTable(sourceTable, filters) {
    sourceTable.querySelectorAll('tbody tr').forEach(row => {
      const shouldShow = shouldShowRow(row, filters);

      if (shouldShow) {
        row.classList.remove("hidden");
      } else {
        row.classList.add("hidden");
      }
    });
  }

  function onInputChange(inputTable, sourceTable) {
    const filters = [];

    inputTable.querySelectorAll('input').forEach((input, index) => {
      const filter = createFilterOrNull(input.value, index);

      if (filter) {
        filters.push(filter);
      }
    });

    filterSourceTable(sourceTable, filters);
  }

  function main() {
    const sourceTable = findSourceTable();
    const parent = sourceTable.parentNode;

    const inputTable = createInputTable(sourceTable);
    inputTable.addEventListener('input', debounce(() => onInputChange(inputTable, sourceTable), 1000));

    parent.prepend(inputTable);
  }
}());
